const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const path = require('path');

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));

mongoose.connect('mongodb+srv://bharath2:test123@bks0.h8v74qu.mongodb.net/FormDB', { useNewUrlParser: true, useUnifiedTopology: true });

const formSchema = new mongoose.Schema({
  username: String,
  password: String
});

const Form = mongoose.model('Form', formSchema);


app.post('/submit', (req, res) => {
  console.log("user: " + req.body.username + " pass: " + req.body.password);
  const formData = new Form({
    username: req.body.username,
    password: req.body.password
  });
  formData.save();
  res.send('Form submitted successfully!');
});

// app.get("/", (req, res) => {
//   res.sendFile(__dirname + "/index.html");
// });

app.listen(5500, () => {
  console.log('Server is running on port 3000');
});
